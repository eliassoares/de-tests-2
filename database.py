from datetime import datetime
from typing import Callable

from mysql.connector import cursor
from pyspark.sql import DataFrame


def save_hosts_in_database(c: cursor, host):
    """
    Salva um host no banco.
    :param c: cursor mysql
    :param host: dado do host a ser salvo
    """
    c.execute(
        '''
        INSERT INTO host(id, host)
        VALUES(%s, %s);
        ''',
        (host.host_id, host.host)
    )


def save_clients_in_database(c: cursor, client):
    """
    Salva um client no banco.
    :param c: cursor mysql
    :param client: dado do client a ser salvo
    """
    c.execute(
        '''
        INSERT INTO client(id, ip)
        VALUES(%s, %s);
        ''',
        (client.client_id, client.client_ip)
    )


def save_services_in_database(c: cursor, service):
    """
    Salva um service no banco.
    :param c: cursor mysql
    :param service: dado do service a ser salvo
    """
    c.execute(
        '''
        INSERT INTO service(id,name,connect_timeout,port,host_id,path,protocol,read_timeout,
            retries,updated_at,write_timeout,created_at)
        VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
        ''',
        (
            service.id,
            service.name,
            service.connect_timeout,
            service.port,
            service.host_id,
            service.path,
            service.protocol,
            service.read_timeout,
            service.retries,
            datetime.fromtimestamp(service.updated_at),
            service.write_timeout,
            datetime.fromtimestamp(service.created_at)
        )
    )


def save_requests_in_database(c: cursor, request):
    """
    Salva um request no banco.
    :param c: cursor mysql
    :param request: dado do request a ser salvo
    """
    c.execute(
        '''
        INSERT INTO request(id,service_id,client_id,host_id,size,method,uri,host_scheme,
            started_at,proxy,kong,request_time)
        VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);
        ''',
        (
            request.id,
            request.service_id,
            request.client_id,
            request.host_id,
            request.size,
            request.method,
            request.uri,
            request.host_scheme,
            datetime.fromtimestamp(request.started_at),
            request.proxy,
            request.kong,
            request.request_time,
        )
    )


def save_dataframe(df: DataFrame, c: cursor, save_func: Callable):
    """
    Dado um dataframe de um dos dados a ser salvo(service, host, client ou request),
    um cursor do banco e uma função de salvamento, como save_requests_in_database,
    itera pelo dataframe chamando a função
    :param df: dataframe de dados
    :param c: cursor do mysql
    :param save_func: função de salvamento a ser executada
    """
    for _, row in df.toPandas().iterrows():
        save_func(c, row)
