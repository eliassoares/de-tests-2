import pyspark.sql.utils
from data_transformation import (
    get_logs_dataframe, get_hosts_dataframe
)
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, StringType, IntegerType
from pyspark_test import assert_pyspark_df_equal
from pytest import raises

from common import schema, data

spark = SparkSession.builder.appName('test').getOrCreate()
fixture_df = spark.createDataFrame(data, schema)


def test_get_logs_dataframe_when_the_file_exists():
    df = get_logs_dataframe(spark, 'tests/logs.test.txt')

    assert_pyspark_df_equal(df, fixture_df)


def test_get_logs_dataframe_when_the_file_not_exists():
    with raises(pyspark.sql.utils.AnalysisException):
        get_logs_dataframe(spark, 'no-one')


def test_get_hosts_dataframe():
    expected_df = spark.createDataFrame(
        [
            {'host': 'conroy.biz', 'host_id': 1},
            {'host': 'ritchie.com', 'host_id': 2},
            {'host': 'schuppe.org', 'host_id': 3},
            {'host': 'terry.org', 'host_id': 4}
        ],
        StructType([
            StructField('host', StringType(), True),
            StructField('host_id', IntegerType(), True),
        ])
    )

    df = get_hosts_dataframe(fixture_df)

    assert_pyspark_df_equal(df, expected_df)

