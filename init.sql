CREATE TABLE host (
	id INT NOT NULL,
	host VARCHAR(2048) NOT NULL,
	PRIMARY KEY(id),
	UNIQUE(host)
);

CREATE TABLE service (
    id VARCHAR(36) PRIMARY KEY,
    name VARCHAR(256),
    connect_timeout INT,
	port INT,
	host_id INT,
	path VARCHAR(256),
	protocol VARCHAR(8),
	read_timeout INT,
	retries INT,
	updated_at TIMESTAMP,
	write_timeout INT,
    created_at TIMESTAMP DEFAULT NOW(),
    FOREIGN KEY (host_id) REFERENCES host(id)
);

CREATE TABLE client (
	id INT NOT NULL,
	ip VARCHAR(15),
	PRIMARY KEY(id)
);

CREATE TABLE request (
	id INT NOT NULL,
	service_id VARCHAR(36),
	client_id INT,
	host_id INT,
	size INT,
	method VARCHAR(7),
	uri VARCHAR(256),
	host_scheme VARCHAR(5),
	querystring VARCHAR(1024) DEFAULT NULL,
	started_at TIMESTAMP,
	proxy INT,
	kong INT,
    request_time INT,
	FOREIGN KEY (service_id) REFERENCES service(id),
	FOREIGN KEY (client_id) REFERENCES client(id),
	FOREIGN KEY (host_id) REFERENCES host(id),
	PRIMARY KEY(id)
);
