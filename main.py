import logging
from os import getenv

import mysql.connector
from pyspark.sql import SparkSession

from data_transformation import (
    get_logs_dataframe, get_hosts_dataframe, get_services_dataframe,
    get_clients_dataframe, get_requests_dataframe
)
from database import (
    save_dataframe, save_clients_in_database, save_hosts_in_database,
    save_requests_in_database, save_services_in_database
)
from reports import (
    generate_client_requests_report, generate_services_requests_report,
    get_report_dataframe, generate_average_time_report
)

db_pass = getenv('MYSQL_PASSWORD')
db_user = getenv('MYSQL_USER')
db_host = getenv('MYSQL_HOST')
db_name = getenv('MYSQL_DATABASE')
db_port = getenv('MYSQL_PORT')

connection = mysql.connector.connect(
    host=db_host,
    database=db_name,
    password=db_pass,
    user=db_user,
    port=db_port
)
cursor = connection.cursor()

spark = SparkSession.builder.appName('melhor_envio').getOrCreate()

filename = 'data/logs.txt'

logs_df = get_logs_dataframe(spark, filename)
hosts_df = get_hosts_dataframe(logs_df)
services_df = get_services_dataframe(logs_df, hosts_df)
clients_df = get_clients_dataframe(logs_df)
requests_df = get_requests_dataframe(logs_df, hosts_df, clients_df)

# Salvando os dados nos bancos:
logging.warning('Começando a popular os dados da tabela client')
save_dataframe(clients_df, cursor, save_clients_in_database)
logging.warning('Finalizando de popular a tabela client')

logging.warning('Começando a popular os dados da tabela host')
save_dataframe(hosts_df, cursor, save_hosts_in_database)
logging.warning('Finalizando de popular a tabela host')

logging.warning('Começando a popular os dados da tabela service')
save_dataframe(services_df, cursor, save_services_in_database)
logging.warning('Finalizando de popular a tabela service')

logging.warning('Começando a popular os dados da tabela request')
save_dataframe(requests_df, cursor, save_requests_in_database)
logging.warning('Finalizando de popular a tabela request')

connection.commit()
cursor.close()
connection.close()

logging.warning('Iniciando a criação dos relatórios csv')
report_df = get_report_dataframe(requests_df, clients_df, services_df)
generate_client_requests_report(report_df)
generate_services_requests_report(report_df)
generate_average_time_report(report_df)
logging.warning('Finalizando a criação dos relatórios csv!')
