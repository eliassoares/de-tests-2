import logging
from functools import reduce
from urllib.parse import urlparse

from pyspark.sql import SparkSession, DataFrame, Window, functions as f
from pyspark.sql.types import StringType


def get_logs_dataframe(spark: SparkSession, filename: str) -> DataFrame:
    """
    Ler um arquivo json e retorna um spark DataFrame
    :param spark: sessão do spark
    :param filename: arquivo para ler
    :return: dataframe lido
    """
    logging.warning('Lendo e tratando os logs')

    df = spark.read.json(filename)

    # Removendo colunas as quais não irei usar
    df = df.drop('authenticated_entity').drop('response') \
        .drop('route').drop('upstream_uri')

    window = Window.orderBy(f.col('started_at'))
    df = df.withColumn('log_id', f.row_number().over(window))

    # Vamos pegar os host de todas as urls das requisições
    udf_host = f.udf(lambda x: urlparse(x).netloc, StringType())
    udf_scheme = f.udf(lambda x: urlparse(x).scheme, StringType())

    df = df.withColumn('host', udf_host(f.col('request.url')).alias('host'))
    df = df.withColumn('host_scheme', udf_scheme(f.col('request.url')).alias('host_scheme'))

    logging.warning('Leitura e tratamento dos arquivos de logs finalizados')

    return df


def get_hosts_dataframe(logs_df: DataFrame) -> DataFrame:
    """
    Extrai os hosts das requisições e dos serviços, de-duplica e
    retorna um dataframe com os hosts, o qual será salvo no banco.
    :param logs_df: dataframe com todos os logs
    :return: dataframe com os hosts extraídos
    """
    logging.warning('Tratando os dados dos hosts')

    # Tanto os serviços quanto as requisições tem hosts ou urls, logo podemos tratar esses dados para
    # salvar na tabela host. A ideia é de-duplicar todos os host, criar id único para cada, e salvar no banco.
    # Usaremos um dataframe com o host e o id para termos a relação da tabela host com as demais
    # tabelas que têm relacionamento com ela.
    request_host_df = logs_df.select('host')
    service_hosts_df = logs_df.select('service.host')

    # Temos dois dataframes com hosts, vamos juntá-los:
    hosts_df = reduce(DataFrame.unionAll, [service_hosts_df, request_host_df])

    # Após a de-duplicação, saímos de 200000 hosts para 2365:
    hosts_df = hosts_df.distinct()

    # Adicionando a coluna id no dataframe
    window = Window.orderBy(f.col('host'))
    hosts_df = hosts_df.withColumn('host_id', f.row_number().over(window))

    logging.warning('Finalizado o tratamento dos dados dos hosts')

    return hosts_df


def get_services_dataframe(logs_df: DataFrame, hosts_df: DataFrame) -> DataFrame:
    """
    Dado o dataframe de logs, retorna um dataframe de services de-duplicados, além
    disso, usa o dataframe de hosts para salvar a relação da url do serviço com
    o id do host.
    :param logs_df: dataframe com todos os logs
    :param hosts_df: dataframe com todos os host
    :return: dataframe de services
    """
    logging.warning('Tratando os dados dos services')

    services_df = logs_df.select('service.*')

    # Para não ter conflito com a coluna do dataframe de hosts, após o join:
    services_df = services_df.withColumnRenamed('host', 'service_host')
    services_df = services_df.drop_duplicates(['id'])

    # Vamos fazer o join do dataframe de service com o de host, para termos o id de cada host
    services_df = services_df.join(hosts_df, services_df.service_host == hosts_df.host)
    services_df = services_df.drop('service_host').drop('host')

    logging.warning('Finalizado o tratamento dos dados dos services')

    return services_df


def get_clients_dataframe(logs_df: DataFrame) -> DataFrame:
    """
    Dado o dataframe de logs, retorna um dataframe de clientes de-duplicados.
    :param logs_df: dataframe com todos os logs
    :return: dataframe de clients
    """
    logging.warning('Tratando os dados dos clients')

    clients_df = logs_df.select('client_ip')
    clients_df = clients_df.distinct()

    # Adicionando a coluna id no dataframe
    window = Window.orderBy(f.col('client_ip'))
    clients_df = clients_df.withColumn('client_id', f.row_number().over(window))

    logging.warning('Finalizado o tratamento dos dados dos clients')

    return clients_df


def get_requests_dataframe(logs_df: DataFrame, hosts_df: DataFrame, clients_df: DataFrame) -> DataFrame:
    """
    Dado os dataframes de logs, hosts e clients, retorna um dataframe de requests pronto
    para ser salvo no banco.
    :param logs_df: dataframe com todos os logs
    :param hosts_df: dataframe com todos os hosts
    :param clients_df: dataframe com todos os clients
    :return: dataframe de requests
    """
    requests_df = logs_df.select(
        [
            'request.*', 'service.id', 'host', 'client_ip', 'host_scheme', 'started_at',
            'latencies.proxy', 'latencies.kong', 'latencies.request', 'log_id'
        ]
    )
    requests_df = requests_df.withColumnRenamed('host', 'request_host')
    requests_df = requests_df.withColumnRenamed('client_ip', 'request_client_ip')
    requests_df = requests_df.withColumnRenamed('id', 'service_id')
    requests_df = requests_df.withColumnRenamed('request', 'request_time')
    requests_df = requests_df.withColumnRenamed('log_id', 'id')

    # Temos as colunas request_host e host_scheme, não precisamos mais da coluna url
    requests_df = requests_df.drop('url')
    requests_df = requests_df.join(hosts_df, requests_df.request_host == hosts_df.host)
    requests_df = requests_df.join(clients_df, requests_df.request_client_ip == clients_df.client_ip)
    requests_df = requests_df.drop('request_host').drop('host')
    requests_df = requests_df.drop('request_client_ip').drop('client_ip')

    # Notei que o campo querystring não tem dado, logo, configurarei
    # como padrão o valor `NULL` no banco e irei remover do dataframe.
    requests_df = requests_df.drop('querystring').drop('headers')

    return requests_df
