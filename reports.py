from pyspark.sql import DataFrame


def get_report_dataframe(requests_df: DataFrame, clients_df: DataFrame, services_df: DataFrame) -> DataFrame:
    """
    Dado os três dataframes requests_df, clients_df e services_df, retorna um dataframe
    o qual os relatórios serão gerados
    :param requests_df: dataframe de requests
    :param clients_df: dataframe de clients
    :param services_df: dataframe de serviços
    :return: dataframe de report
    """
    report_df = requests_df.join(clients_df, requests_df.client_id == clients_df.client_id)
    report_df = report_df.join(services_df, report_df.service_id == services_df.id)

    return report_df


def generate_client_requests_report(report_df: DataFrame):
    """
    Dado o dataframe com os dados de reports, cria um novo dataframe com as estatísticas de requisições
    por consumidor e salva no arquivo client_requests.csv
    :param report_df: dataframe com os dados
    """
    client_requests = report_df.groupBy('client_ip').count().withColumnRenamed('count', 'client_requests')
    client_requests = client_requests.orderBy(client_requests['client_requests'].desc())
    client_requests.toPandas().to_csv('data/client_requests.csv')


def generate_services_requests_report(report_df: DataFrame):
    """
    Dado o dataframe com os dados de reports, cria um novo dataframe com as estatísticas de requisições
    por serviços e salva no arquivo service_requests.csv
    :param report_df: dataframe com os dados
    :return:
    """
    service_requests = report_df.groupBy('service_id').count().withColumnRenamed('count', 'service_requests')
    service_requests = service_requests.orderBy(service_requests['service_requests'].desc())
    service_requests.toPandas().to_csv('data/service_requests.csv')


def generate_average_time_report(report_df: DataFrame):
    """
    Dado o dataframe com os dados de reports, cria um novo dataframe com as estatísticas de média de tempos de request,
    do proxy e do kong, agregados por serviço, salva os dados do dataframe no arquivo services_avg.csv
    :return:
    """
    services_mean_df = report_df.groupBy('service_id').agg({'request_time': 'avg', 'proxy': 'avg', 'kong': 'avg'})
    services_mean_df.toPandas().to_csv('data/services_mean.csv')


