# Solução
## Introdução
Nesse projeto modelei um banco de forma relacional, buscando desde o início deixar o modelo normalizado,
processe um grande arquivo de logs, salvei os dados no banco que foi modelado e gerei alguns relatórios, os
salvando em arquivos csvs. 

**Esse projeto foi desevolvido no pouco tempo livre que tive durante essa semana.**

## Tecnologias usadas
Como banco de dados utilizei o que recomendado pela descrição do problema, o MySQL, para processar o arquivos, 
salvar os dados no banco e gerar os relatórios utilizei o [PySpark](http://spark.apache.org/docs/latest/api/python/),
biblioteca muito utilizada para processamento distribuído na engenharia de dados.

Essa solução foi desenvolvida no sistema operacional Debian, mas desde o início já criei para que fosse
facilmente replicável na máquina de qualquer desenvolvedor, para isso, utilizei o 
[Docker](https://docs.docker.com/desktop/) como ferramenta de conteinização. Logo, *basta* ter o Docker 
instalado para executar o programa. Ter o [Make](https://opensource.com/article/18/8/what-how-makefile) instalado
é opcional, mas criei um arquivo Makefile com alguns comandos atalhos. Quando explicar como executar o teste, 
colocaria o comando completo e o "atalho" do make.

## Arquivos
- `data_transformation.py`: arquivo que contêm as funções que irão manipular e transformar os dados;
- `database.py`: arquivo que contêm as funções responsáveis para lidar com o banco e salvar os dados;
- `reports.py`: arquivo que contêm as funções que irão manipular os dados, gerar os relatórios e salvá-los em csv;
- `main.py`: arquivo principal que chamará as funções na ordem correta;
- `Dockerfile`: arquivo Dockerfile usado para criar uma imagem docker customizada, baseada na `mysql-connector-python`,
afim de instalar a biblioteca responsável por se comunicar com o MySQL;
- `Dockerfile-DB`: arquivo Dockerfile usado para criar do MySQL, foi customizada para já inicializar o
banco com as tabelas e banco criado;
- `Exploration.ipynb`: arquivo do [jupyter notebook](https://jupyter.org/), o qual fiz uma exploração e solução inicial
do problema, lá contêm alguns comentários e "prints" extras;
- `init.sql`: arquivo com as queries DDL, o qual cria as tabelas;
- `Makefile`: arquivo make o qual contêm os "atalhos" para os comandos para executar o código.

## Modelagem
Desde o início já tentei modelar os dados de forma mais normalizada possível. Além disso, salvei
apenas as tabelas e dados necessários para gerar os relatórios requeridos.

![Arquitetura](model.png)

No arquivo `init.sql` é possível encontrar as queries de criação das tabelas.

## Execução do código
### Sem make
Primeiramente é necessário criar as imagens docker:
```shell
docker build -f Dockerfile -t pyskpark .
```
```shell
docker build -t mysql-melhor-envio -f Dockerfile-DB .
```

Suba o container do banco:
```shell
docker run -d --rm -e MYSQL_DATABASE=melhor_envio -e MYSQL_USER=arya-stark -e MYSQL_PASSWORD=not-today \
    -e MYSQL_ROOT_PASSWORD=nymeria --name=mysql-melhor-envio -p "33306:3306" \
    mysql-melhor-envio
```

Após alguns segundos, cerca de 30, execute o comando para executar a solução:
```shell
docker run --rm -ti -v $PWD:/home/jovyan/work --network host -e MYSQL_HOST=localhost \
		-e MYSQL_DATABASE=melhor_envio -e MYSQL_USER=arya-stark -e MYSQL_PASSWORD=not-today \
		-e MYSQL_PORT=33306 pyskpark bash -c "cd /home/jovyan/work; pyspark < main.py"
```

### Com make
```shell
make run-code
```

### Avaliando a solução
Após a execução do comando, o script começará ser executado, adicionei alguns logs para auxiliar
o andamento da execução. Ao final, os dados serão inseridos no banco, e três arquivos serão criados na pasta data: 
`client_requests.csv`, `service_requests.csv` e `services_mean.csv`.

Você pode acessar o banco em qualquer IDE, como Datagrip e Dbeaver, as credenciais são:
- `MYSQL_DATABASE`: melhor_envio
- `MYSQL_USER`: arya-stark
- `MYSQL_HOST`: localhost
- `MYSQL_PASSWORD`: not-today

Acessando o banco é possível verificar se os dados foram inseridos.

Outra forma de gerar os relatórios, mas sem gerar os csvs, é via SQL, abaixo deixo as
queries para cada relatório.

**Requisições por consumidor**
```sql
SELECT COUNT(*) AS client_requests, c.ip 
FROM request r 
INNER JOIN client c 
ON r.client_id = c.id
GROUP BY c.ip
ORDER BY client_requests DESC;
```

**Requisições por serviço**
```sql
SELECT COUNT(*) AS services_requests, s.id, s.name 
FROM request r 
INNER JOIN service s 
ON r.service_id = s.id
GROUP BY s.id
ORDER BY services_requests DESC;
```

**Tempo médio de request, proxy e kong por serviço**
```sql
SELECT s.id, s.name, AVG(r.proxy), AVG(r.request_time), AVG(r.kong) 
FROM request r 
INNER JOIN service s 
ON r.service_id = s.id
GROUP BY s.id;
```

## O que poderia ser melhor
- Ao salvar os dados no banco, eu iterei nos dados do dataframe, essa forma é menos eficiente do 
que usar, por exemplo a função do `to_sql` do Pandas, entretanto, tive alguns problemas técnicos para
fazer essa solução funcionar, por questão de tempo, optei por resolver o problema sem essa otimização;
- Para executar a solução, é necessário esperar alguns segundos para inicialização e criação das tabelas 
do container do MySQL, forcei uma espera de 30 segundos no Makefile, mas acho que essa solução poderia ser melhor.
- Comecei a fazer os testes, mas por conta do tempo tive que parar e tem pouquíssimos testes.
Se tivesse mais tempo faria mais testes. 

## Referências
- https://towardsdatascience.com/adding-sequential-ids-to-a-spark-dataframe-fa0df5566ff6
- https://walkenho.github.io/merging-multiple-dataframes-in-pyspark/
- https://stackoverflow.com/questions/16476413/how-to-insert-pandas-dataframe-via-mysqldb-into-database
- https://www.datasciencemadesimple.com/mean-of-two-or-more-columns-in-pyspark/
