DB_PASS=not-today
DB_ROOT_PASS=nymeria
DB_HOST=localhost
DB_PORT=33306
DB_USER=arya-stark
DB_NAME=melhor_envio
PYSPARK_WORK_PARK=/home/jovyan/work

build-pyspark:
	docker build -f Dockerfile -t pyskpark .

build-mysql:
	docker build -t mysql-melhor-envio -f Dockerfile-DB .

run-jupyter: build-pyspark run-mysql
	docker run --rm --name=pyspark -p 8888:8888 -p 8889:8889 -v $(PWD):${PYSPARK_WORK_PARK} --network host \
		-e GRANT_SUDO=yes --user root pyskpark

run-mysql: build-mysql
	docker run -d --rm -e MYSQL_DATABASE=${DB_NAME} -e MYSQL_USER=${DB_USER} -e MYSQL_PASSWORD=${DB_PASS} \
		-e MYSQL_ROOT_PASSWORD=${DB_ROOT_PASS} --name=mysql-melhor-envio -p "${DB_PORT}:3306" \
		mysql-melhor-envio
	echo "Esperando 30 segundos para o mysql se inicializar"
	sleep 30

run-code: build-pyspark run-mysql
	docker run --rm -ti -v $(PWD):${PYSPARK_WORK_PARK} --network host -e MYSQL_HOST=${DB_HOST} \
		-e MYSQL_DATABASE=${DB_NAME} -e MYSQL_USER=${DB_USER} -e MYSQL_PASSWORD=${DB_PASS} \
		-e MYSQL_PORT=${DB_PORT} pyskpark bash -c "cd ${PYSPARK_WORK_PARK}; pyspark < main.py"

run-test:
	docker run --rm -ti -v $(PWD):${PYSPARK_WORK_PARK} --network host -e MYSQL_HOST=${DB_HOST} \
		-e MYSQL_DATABASE=${DB_NAME} -e MYSQL_USER=${DB_USER} -e MYSQL_PASSWORD=${DB_PASS} \
		-e MYSQL_PORT=${DB_PORT} -e PYTHONPATH=${PYSPARK_WORK_PARK} pyskpark bash -c "cd ${PYSPARK_WORK_PARK}; pytest tests"

stop-all:
	docker stop pyskpark mysql-melhor-envio
