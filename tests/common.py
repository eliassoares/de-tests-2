from pyspark.sql.types import (
    StructType, StructField, StringType, IntegerType, LongType, ArrayType
)

schema = StructType([
    StructField('client_ip', StringType(), True),
    StructField('latencies', StructType([
        StructField('kong', LongType(), True),
        StructField('proxy', LongType(), True),
        StructField('request', LongType(), True),
    ])),
    StructField('request', StructType([
        StructField('headers', StructType([
            StructField('accept', StringType(), True),
            StructField('host', StringType(), True),
            StructField('user-agent', StringType(), True),
        ])),
        StructField('method', StringType(), True),
        StructField('querystring', ArrayType(StringType()), True),
        StructField('size', LongType(), True),
        StructField('uri', StringType(), True),
        StructField('url', StringType(), True),
    ])),
    StructField('service', StructType([
        StructField('connect_timeout', LongType(), True),
        StructField('created_at', LongType(), True),
        StructField('host', StringType(), True),
        StructField('id', StringType(), True),
        StructField('name', StringType(), True),
        StructField('path', StringType(), True),
        StructField('port', LongType(), True),
        StructField('protocol', StringType(), True),
        StructField('read_timeout', LongType(), True),
        StructField('retries', LongType(), True),
        StructField('updated_at', LongType(), True),
        StructField('write_timeout', LongType(), True),
    ])),
    StructField('started_at', LongType(), True),
    StructField('log_id', IntegerType(), True),
    StructField('host', StringType(), True),
    StructField('host_scheme', StringType(), True),
])

data = [
    {
        'request': {
            'method': 'GET', 'uri': '/', 'url': 'http://conroy.biz', 'size': 155, 'querystring': [],
            'headers': {
                'accept': '*/*', 'host': 'conroy.biz', 'user-agent': 'curl/7.37.1'
            }
        },
        'service': {
            'connect_timeout': 60000, 'created_at': 1547309158, 'host': 'terry.org',
            'id': 'd035ffcf-914a-3007-b028-ae18f04d75b4', 'name': 'terry', 'path': '/', 'port': 80,
            'protocol': 'http', 'read_timeout': 60000, 'retries': 5, 'updated_at': 1547309158,
            'write_timeout': 60000
        },
        'latencies': {'proxy': 872, 'kong': 5, 'request': 1877},
        'client_ip': '160.226.25.22', 'started_at': 1552852682,
        'log_id': 1,
        'host': 'conroy.biz',
        'host_scheme': 'http',
    },
    {
        'request': {
            'method': 'GET', 'uri': '/', 'url': 'http://schuppe.org', 'size': 142, 'querystring': [],
            'headers': {
                'accept': '*/*', 'host': 'schuppe.org', 'user-agent': 'curl/7.37.1'
            }
        },
        'service': {
            'connect_timeout': 60000, 'created_at': 1563589483, 'host': 'ritchie.com',
            'id': 'c3e86413-648a-3552-90c3-b13491ee07d6', 'name': 'ritchie', 'path': '/', 'port': 80,
            'protocol': 'http', 'read_timeout': 60000, 'retries': 5, 'updated_at': 1563589483,
            'write_timeout': 60000
        },
        'latencies': {'proxy': 979, 'kong': 20, 'request': 1262},
        'client_ip': '215.120.17.208', 'started_at': 1567310808,
        'log_id': 2,
        'host': 'schuppe.org',
        'host_scheme': 'http',
    }
]
